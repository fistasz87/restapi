1) create REST API application using Django Rest Framework
2) application should have model classes: User and Company
3) User fields: username, password, list of roles
4)  Company fields: name, tel, email
5) Provide additional endpoint to add new user  POST /api/extra/users should be no permission checkin for this endpoint
6) Support two Roles Admin and Manager
7) Admin can create and modify  Admins and Managers
7b) Managers can't edit users
8) Admin can create Company
9) Manager can only update Company information
10) authentication should be jwt based
11) Add tests to the application
