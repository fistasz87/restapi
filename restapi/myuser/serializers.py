from django.contrib.auth.models import Group

from rest_framework import serializers

from restapi.myuser.models import MyUser


class MyUserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = MyUser
        extra_kwargs = {
            'password': {'write_only': True},
            'groups': {'view_name': 'group'},
        }
        fields = ('uuid', 'username', 'password', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        lookup_field = 'id'
        fields = ('id', 'name')
