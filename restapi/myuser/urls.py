from django.urls import path
from restapi.myuser.views import (ExtraMyUserAPIView, GroupListCreateAPIView,
                                  GroupRetrieveUpdateDestroyAPIView,
                                  MyUserListCreateAPIView,
                                  MyUserRetrieveUpdateDestroyAPIView)

urlpatterns = [
    path('myuser/', MyUserListCreateAPIView.as_view(), name='myuser'),
    path('group/', GroupListCreateAPIView.as_view(), name='group'),
    path(
        'myuser/<uuid:uuid>/',
        MyUserRetrieveUpdateDestroyAPIView.as_view(),
        name='myuser',
    ),
    path(
        'myuser/<int:pk>/',
        GroupRetrieveUpdateDestroyAPIView.as_view(),
        name='group',
    ),
    path(
        'extra/myuser/',
        ExtraMyUserAPIView.as_view(),
        name='extra_user',
    )
]
