from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import MyUser
from .serializers import GroupSerializer, MyUserSerializer


class MyUserListCreateAPIView(ListCreateAPIView):
    queryset = MyUser.objects.all()
    serializer_class = MyUserSerializer
    lookup_field = 'uuid'


class MyUserRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = MyUser.objects.all()
    serializer_class = MyUserSerializer
    lookup_field = 'uuid'


class GroupListCreateAPIView(ListCreateAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class GroupRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class ExtraMyUserAPIView(APIView):
    permission_classes = []

    def post(self, request, format=None):
        serializer = MyUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
