from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .models import MyUser


class MyUserAdmin(UserAdmin):
    model = MyUser
    list_display = ['username']


admin.site.register(MyUser, MyUserAdmin)
