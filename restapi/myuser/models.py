import uuid as uuid_lib

from django.contrib.auth.models import AbstractUser
from django.db import models
from restapi.myuser.const import ROLE_CHOICES


class MyUser(AbstractUser):
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
