from django.contrib.auth.models import Permission
from django.test import TestCase, tag
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from ..models import MyUser
from ..views import (ExtraMyUserAPIView, MyUserListCreateAPIView,
                     MyUserRetrieveUpdateDestroyAPIView)
from .factories import (AdminGroupFactory, ManagerGroupFactory,
                        MyUserDictFactory, MyUserFactory)


class MixInMyUserAPIViewTestCase(TestCase):

    def setUp(self):
        self._user_data = MyUserDictFactory.create()
        self._factory = APIRequestFactory()
        self._url = reverse('myuser')
        admin_group = AdminGroupFactory.create()
        self._admin_user = MyUserFactory.create(groups=(admin_group,))
        manager_group = ManagerGroupFactory.create()
        self._manager_user = MyUserFactory.create(groups=(manager_group,))


@tag('integration')
class MyUserListCreateAPIViewTestCase(MixInMyUserAPIViewTestCase):

    def setUp(self):
        super().setUp()
        self._view = MyUserListCreateAPIView.as_view()

    def test_create_new_user_as_admin(self):
        # Arrangement
        request = self._factory.post(self._url, self._user_data)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_create_new_user_as_manager_return_forbidden(self):
        # Arrangement
        request = self._factory.post(self._url, self._user_data)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_users_list(self):
        # Arrangement
        size = 5
        MyUserFactory.create_batch(size)
        request = self._factory.get(self._url)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(len(response.data), 7)


@tag('integration')
class MyUserRetrieveUpdateDestroyAPIViewTestCase(MixInMyUserAPIViewTestCase):

    def setUp(self):
        super().setUp()
        size = 5
        self._users = MyUserFactory.create_batch(size)
        self._user = self._users[2]
        self._view = MyUserRetrieveUpdateDestroyAPIView.as_view()

    def test_get_user(self):
        # Arrangement
        request = self._factory.get(self._url)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request, uuid=self._user.uuid)

        # Assert
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        for key, value in response.data.items():
            if key == 'groups':
                self.assertEqual(
                    list(getattr(self._user, key).all()),
                    value,
                )
            else:
                self.assertEqual(
                    str(getattr(self._user, key)),
                    value,
                )

    def test_put_user(self):
        # Arrangement
        new_name = 'new_name'
        data = {
            'username': new_name,
            'email': self._user.email,
            'password': 'aa'
        }
        request = self._factory.put(self._url, data)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request, uuid=self._user.uuid)

        # Assert
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data['username'], new_name)

    def test_put_user_as_manager_return_forbidden(self):
        # Arrangement
        new_name = 'new_name'
        data = {
            'username': new_name,
            'email': self._user.email,
            'password': 'aa'
        }
        request = self._factory.put(self._url, data)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request, uuid=self._user.uuid)

        # Assert
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_delete_user(self):
        # Arrangement
        request = self._factory.delete(self._url)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request, uuid=self._user.uuid)

        # Assert
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        with self.assertRaises(MyUser.DoesNotExist):
            MyUser.objects.get(uuid=self._user.uuid)

    def test_delete_user_as_manager_return_forbbiden(self):
        # Arrangement
        request = self._factory.delete(self._url)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request, uuid=self._user.uuid)

        # Assert
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)


@tag('integration')
class ExtraMyUserAPIViewTestCase(MixInMyUserAPIViewTestCase):

    def setUp(self):
        self._user_data = MyUserDictFactory.create()
        self._factory = APIRequestFactory()
        self._url = reverse('extra_user')
        self._view = ExtraMyUserAPIView.as_view()

    def test_create_new_user(self):
        # Arrangement
        request = self._factory.post(self._url, self._user_data)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
