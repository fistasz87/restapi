import factory
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from restapi.company.models import Company

from ..models import MyUser


class MixInFactory(factory.Factory):

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        obj = model_class(*args, **kwargs)
        obj.save()
        return obj


class AdminGroupFactory(MixInFactory):
    name = 'admin'

    class Meta:
        model = Group

    @factory.post_generation
    def permissions(self, create, extracted, **kwargs):
        if not create:
            return

        content_types = [ct.pk for ct in ContentType.objects.get_for_models(
            Company, MyUser).values()]
        permissions = Permission.objects.filter(
            content_type__in=content_types
        ).values_list('pk', flat=True)
        self.permissions.set(permissions)


class ManagerGroupFactory(MixInFactory):
    name = 'manager'

    class Meta:
        model = Group

    @factory.post_generation
    def permissions(self, create, extracted, **kwargs):
        if not create:
            return

        content_type = ContentType.objects.get_for_model(Company)
        permissions = Permission.objects.filter(
            content_type=content_type).exclude(
            codename__in=['add_company', 'delete_company']
        ).values_list('pk', flat=True)
        self.permissions.set(permissions)


class MyUserMixInFactory(factory.Factory):
    username = factory.Sequence(lambda n: 'username_{}'.format(n))
    email = factory.Sequence(lambda n: 'email{}@com.pl'.format(n))
    password = 'pass1234'


class MyUserFactory(MyUserMixInFactory):
    is_superuser = False
    groups = factory.List([
          factory.RelatedFactory(ManagerGroupFactory),
    ])

    class Meta:
        model = MyUser

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        obj = model_class(*args, **kwargs)
        obj.save()
        return obj

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for group in extracted:
                self.groups.add(group)


class MyUserDictFactory(MyUserMixInFactory):
    groups = []

    class Meta:
        model = dict
