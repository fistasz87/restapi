from django.urls import path

from restapi.company.views import (
    CompanyListCreateAPIView,
    CompanyRetrieveUpdateDestroyAPIView,
)


urlpatterns = [
    path('company/', CompanyListCreateAPIView.as_view(), name='company'),
    path(
        'company/<uuid:uuid>/',
        CompanyRetrieveUpdateDestroyAPIView.as_view(),
        name='company',
    ),
]
