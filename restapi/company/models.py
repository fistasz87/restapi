import uuid as uuid_lib

from django.db import models
from phone_field import PhoneField


class Company(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    name = models.CharField(max_length=255)
    tel = PhoneField(blank=True)
    email = models.EmailField(max_length=255)

    def __str__(self):
        return '{} name: {}, uuid: {}'.format(
            self.__class__.__name__,
            self.name,
            self.uuid,
        )
