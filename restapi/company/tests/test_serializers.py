from django.test import TestCase, tag
from restapi.company.const import PHONE_VALIDATION_ERROR
from restapi.company.serializers import CompanySerializer
from restapi.company.tests.factories import CompanyDictFactory


@tag('unit')
class CompanySerializerTestCase(TestCase):

    def setUp(self):
        self._company = CompanyDictFactory.create()
        self._serializer = CompanySerializer(data=self._company)

    def test_correct_serialization(self):
        # Act/Assert
        self.assertTrue(self._serializer.is_valid())

    def test_invalid_number(self):
        # Arrangement
        self._company.update({'tel': 'dasdas'})

        # Act
        self._serializer.is_valid()
        errors = self._serializer.errors

        # Assert
        self.assertEqual(str(errors['tel'][0]), PHONE_VALIDATION_ERROR)
