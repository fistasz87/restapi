import factory
import factory.fuzzy

from restapi.company.models import Company


class CompanyMixInFactory(factory.Factory):
    name = factory.Sequence(lambda n: 'name_{}'.format(n))
    tel = factory.fuzzy.FuzzyInteger(100000000, 999999999)
    email = factory.Sequence(lambda n: 'email{}@com.pl'.format(n))


class CompanyFactory(CompanyMixInFactory):
    class Meta:
        model = Company

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        obj = model_class(*args, **kwargs)
        obj.save()
        return obj


class CompanyDictFactory(CompanyMixInFactory):
    class Meta:
        model = dict
