from django.test import TestCase, tag
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from restapi.company.models import Company
from restapi.company.views import (CompanyListCreateAPIView,
                                   CompanyRetrieveUpdateDestroyAPIView)
from restapi.myuser.tests.factories import (AdminGroupFactory,
                                            ManagerGroupFactory, MyUserFactory)

from .factories import CompanyDictFactory, CompanyFactory


@tag('integration')
class CompanyListCreateAPIViewTestCase(TestCase):

    def setUp(self):
        self._company_data = CompanyDictFactory.create()
        self._factory = APIRequestFactory()
        self._url = reverse('company')
        self._view = CompanyListCreateAPIView.as_view()
        group = ManagerGroupFactory.create()
        self._manager_user = MyUserFactory.create(groups=(group,))
        admin_group = AdminGroupFactory.create()
        self._admin_user = MyUserFactory.create(groups=(admin_group,))

    def test_create_new_company(self):
        # Arrangement
        request = self._factory.post(self._url, self._company_data)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_create_new_company_as_manager_return_forbidden(self):
        # Arrangement
        request = self._factory.post(self._url, self._company_data)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_validation_error(self):
        # Arrangement
        self._company_data['email'] = 'adasdas'
        request = self._factory.post(self._url, self._company_data)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(response.data['email'][0].code, 'invalid')

    def test_get_companies_list(self):
        # Arrangement
        size = 5
        CompanyFactory.create_batch(size)
        request = self._factory.get(self._url)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request)

        # Assert
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(len(response.data), size)


@tag('integration')
class CompanyRetrieveUpdateDestroyAPIViewTestCase(TestCase):

    def setUp(self):
        size = 5
        self._companies = CompanyFactory.create_batch(size)
        self._company = self._companies[2]
        self._company_data = CompanyDictFactory.create()
        self._factory = APIRequestFactory()
        self._url = reverse('company')
        self._view = CompanyRetrieveUpdateDestroyAPIView.as_view()
        group = ManagerGroupFactory.create()
        self._manager_user = MyUserFactory.create(groups=(group,))
        admin_group = AdminGroupFactory.create()
        self._admin_user = MyUserFactory.create(groups=(admin_group,))

    def test_get_company(self):
        # Arrangement
        request = self._factory.get(self._url)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request, uuid=self._company.uuid)

        # Assert
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        for key, value in response.data.items():
            self.assertEqual(
                str(getattr(self._company, key)),
                value,
            )

    def test_put_company(self):
        # Arrangement
        new_name = 'new_name'
        data = {
            'name': new_name,
            'email': self._company.email,
            'tel': self._company.tel,
        }
        request = self._factory.put(self._url, data)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request, uuid=self._company.uuid)

        # Assert
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data['name'], new_name)

    def test_delete_company(self):
        # Arrangement
        request = self._factory.delete(self._url)
        force_authenticate(request, user=self._admin_user)

        # Act
        response = self._view(request, uuid=self._company.uuid)

        # Assert
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        with self.assertRaises(Company.DoesNotExist):
            Company.objects.get(uuid=self._company.uuid)

    def test_delete_company_as_manager_forbidden(self):
        # Arrangement
        request = self._factory.delete(self._url)
        force_authenticate(request, user=self._manager_user)

        # Act
        response = self._view(request, uuid=self._company.uuid)

        # Assert
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
