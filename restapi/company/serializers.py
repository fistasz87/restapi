from rest_framework import serializers
from restapi.company.const import PHONE_VALIDATION_ERROR
from restapi.company.models import Company


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    tel = serializers.CharField(max_length=100)

    class Meta:
        model = Company
        fields = ['name', 'uuid', 'email', 'tel']

    def validate_tel(self, value):
        if value.isdigit():
            return value
        raise serializers.ValidationError(PHONE_VALIDATION_ERROR)
